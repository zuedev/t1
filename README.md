<img src="assets/images/t1_logo.png" height="150px"/>

# Termina One, Web Wizards 🧙

> Utilizing Open-Source magic to transform digital businesses. 🪄✨

**Termina One** is a group of nerds founded by [zuedev](https://zue.dev) known for innovating the development of software and hardware for the tech industry. We are a team of professionals that have been working in the tech industry for over 10 years. We have worked with many companies and have helped them to develop their products and services.
